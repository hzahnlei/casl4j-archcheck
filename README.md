# CASL4J - Clean Architecture Support Library for Java - Architecture Check

Gradle plug-in to ensure Java applications are compliant to Clean Architecture design rules.
Part of Clean Architecture Support Library for Java.

2022-05-28, Holger Zahnleiter

## Introduction

This is a custom Gradle plug-in that can be used to make sure your Java code is comliant to the rules of a Clean Architecture.
The plug-in only verifies the domain logic code.
It is not aware of the technical application code as such checks depend on concrete technologies.
Actually, the Spring implementation of CAS4J contains an additional plug-in that checks the technical layers.
However, both plug-ins are optional and not required if you do not want to use them.

## How to Add this Gradle Plug-in to Your Project

### Cloning and Downloading

Of cause you can clone this project, compile it and push the artifacts into your local Maven repository and use it from there.

Furtheremore, the artifacts can be downloaded from GitLab.
Do so and add the JAR to your project's class-path.

### Maven Repository

The preferred way is to add the artifacts (JAR, source and Javadoc) to your project as Maven or Gradle dependencies.
This requires that you first add my GitLab Maven repository to your Gradle or Maven build file like shown next.

We cannot just activate this plug-in by adding its ID to the `plugins` section of `build.gradle` as this is not a default Gradle plug-in.

In **settings.gradle** you have to introduce my custom repositories from which to pull the plug-in:

```groovy
pluginManagement {
	repositories {
        ...
        // Requires all my GitLab repos that belog to my CASL4J project.
        maven {
            url 'https://gitlab.com/api/v4/projects/36778618/packages/maven'
            name '_CASL4J_GitLab_'
        }
        maven {
            url 'https://gitlab.com/api/v4/projects/36830434/packages/maven'
            name '_CASL4J-archcheck_GitLab_'
        }
        ...
    }
}
```

In **build.gradle** you configure the architecture check plug-in itself:

```groovy
plugins {
    ...
    id 'org.zahnleiter.casl4j.domain-architecture-check' version '1.0.0'
    ...
}
```

The plug-in ID is not sufficient, as this is no core plug-in but a custom plug-in.
The version needs to be provided as well.

You can now execute the architecture checks with the following Gradle task

```bash
./gradlew domainArchitectureCheck
```

## Disclaimer

This is a private, free, open source and non-profit project (see LICENSE file).
Use on your own risk.
I am not liable for any damage caused.

I am a private person, not associated with any companies mentioned herein.

## Credits

The ideas presented herein are not my own.
I am building on the ideas and experience of countless software developers and software architects before me.
Also, I am using many open source and free software and services.
Thank you:

-  [GitLab](https://gitlab.com)
-  [Visual Studio Code](https://code.visualstudio.com) - Microsoft
-  [Eclipse](https://www.eclipse.org/downloads/) - Eclipse Foundation
-  [Git](https://git-scm.com)
-  [Gradle](https://gradle.org)
-  [Maven](https://maven.apache.org) - Apache Software Foundation
-  [ArchUnit](https://www.archunit.org) - TNG Technology Consulting GmbH
-  Many more that I might have forgotten or even not be aware of using...

## Technical Note on Gradle Plug-In Development and JDK 17

This projects delivers a Gradle plug-in to perform architecture checks.
It also contains related tests, of cause.
However, I could not execute the integration test for the plug-in from Visual Studio Code's Test Explorer.
According to this error description, the problem seems to be there since JDK 16: https://github.com/gradle/gradle/issues/18647.
It is related to accessing modules and the Gradle test fixture seems not to be fit for JDK16+.

This is not the only problem with this test.
I am not providing a `src/main/resources/META-INF/gradle-plugins/org.zahnleiter.casl4j.domain-architecture-check.properties` as I am using Gradle's plug-in publishing plug-in.
But, agian, Gradle's own test ficture for plug-in tests seems to require the properties file.

For now, I have deactivated this one integration test.
Furthermore I stick with Java 11.
Maybe switching to Gradle 8 will eliminate the problem in the future.
