package org.zahnleiter.casl4j.archcheck;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.zahnleiter.casl4j.archcheck.domain.DomainEntityInCorrectPackage;
import org.zahnleiter.casl4j.archcheck.domain.exception.DomainExceptionInCorrectPackage;
import org.zahnleiter.casl4j.archcheck.wrongpackage.DomainEntityInWrongPackage;
import org.zahnleiter.casl4j.archcheck.wrongpackage.exception.DomainExceptionInWrongPackage;

import com.tngtech.archunit.core.importer.ClassFileImporter;

/**
 * Clean Architecture Support Library for Java - Architecture Check
 * <br>
 * (c) 2021 Holger Zahnleiter, all rights reserved
 *
 * @author Holger Zahnleiter
 */
@Tag("UT")
@Tag("fast")
@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
final class PackageConventionsUT {

    final ClassFileImporter classImporter = new ClassFileImporter();

    @Test
    void DomainException_in_wrong_package() {
        final var myClasses = this.classImporter.importClasses(DomainExceptionInWrongPackage.class);
        try {
            PackageConventions.domainExceptionInDomainExceptionPackage.check(myClasses);
            fail("Expected exception");
        } catch (final AssertionError cause) {
            assertEquals(
                    "Architecture Violation [Priority: MEDIUM] - Rule 'classes that are assignable to org.zahnleiter.casl4j.domain.exception.AbstractDomainException should reside in any package ['..domain..', '..domain.exception..']' was violated (1 times):\n"
                            + "Class <org.zahnleiter.casl4j.archcheck.wrongpackage.exception.DomainExceptionInWrongPackage> does not reside in any package ['..domain..', '..domain.exception..'] in (DomainExceptionInWrongPackage.java:0)",
                    cause.getMessage());
        }
    }

    @Test
    void DomainException_in_correct_package() {
        final var myClasses = this.classImporter.importClasses(DomainExceptionInCorrectPackage.class);
        assertDoesNotThrow(() -> PackageConventions.domainExceptionInDomainExceptionPackage.check(myClasses));
    }

    @Test
    void DomainEntity_in_wrong_package() {
        final var myClasses = this.classImporter.importClasses(DomainEntityInWrongPackage.class);
        try {
            PackageConventions.domainEntitiesInDomainPackage.check(myClasses);
            fail("Expected exception");
        } catch (final AssertionError cause) {
            assertEquals(
                    "Architecture Violation [Priority: MEDIUM] - Rule 'classes that are annotated with @DomainEntity should reside in any package ['..domain..', '..domain.entity..']' was violated (1 times):\n"
                            + "Class <org.zahnleiter.casl4j.archcheck.wrongpackage.DomainEntityInWrongPackage> does not reside in any package ['..domain..', '..domain.entity..'] in (DomainEntityInWrongPackage.java:0)",
                    cause.getMessage());
        }
    }

    @Test
    void DomainEntity_in_correct_package() {
        final var myClasses = this.classImporter.importClasses(DomainEntityInCorrectPackage.class);
        assertDoesNotThrow(() -> PackageConventions.domainEntitiesInDomainPackage.check(myClasses));
    }

}
