package org.zahnleiter.casl4j.archcheck.domain.exception;

import java.time.OffsetDateTime;
import java.util.UUID;

import org.zahnleiter.casl4j.domain.exception.AbstractDomainException;

public final class DomainExceptionInCorrectPackage extends AbstractDomainException {

    public DomainExceptionInCorrectPackage(String message, UUID id, OffsetDateTime timeOfOccurrence) {
        super(message, id, timeOfOccurrence);
    }

}
