package org.zahnleiter.casl4j.archcheck;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

/**
 * Clean Architecture Support Library for Java - Architecture Check
 * <br>
 * (c) 2021 Holger Zahnleiter, all rights reserved
 *
 * @author Holger Zahnleiter
 */
@Tag("UT")
@Tag("fast")
@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
final class DomainCheckGradlePluginUT {

    @Test
    void Verify_Gradle_task_name() {
        final var pluginUnderTest = new DomainCheckGradlePlugin();
        assertEquals("domainArchitectureCheck", pluginUnderTest.taskName(),
                "If this is ever gonna change, then all depending Gradle projects need to be adapted.");
    }

}
