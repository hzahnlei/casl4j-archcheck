package org.zahnleiter.casl4j.archcheck;

import org.junit.jupiter.api.Test;
import org.zahnleiter.casl4j.archcheck.wrongpackage.DomainEntityInWrongPackage;

import com.tngtech.archunit.core.importer.ClassFileImporter;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.Tag;

@Tag("CT")
@Tag("fast")
@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
final class DomainCheckGradlePlugInCT {

    final ClassFileImporter classImporter = new ClassFileImporter();

    @Test
    void Plugin_performs_domain_checks() {
        final var pluginUnderTest = new DomainCheckGradlePlugin();
        final var myClasses = this.classImporter.importClasses(DomainEntityInWrongPackage.class);
        assertThrows(AssertionError.class, () -> pluginUnderTest.architectureChecks(myClasses));
    }

    @Test
    void Classes_that_are_not_in_domain_do_not_cause_violations() {
        final var pluginUnderTest = new DomainCheckGradlePlugin();
        final var myClasses = this.classImporter.importClasses(DomainCheckGradlePlugInCT.class);
        assertDoesNotThrow(() -> pluginUnderTest.architectureChecks(myClasses));
    }

}
