package org.zahnleiter.casl4j.archcheck.wrongpackage.exception;

import java.time.OffsetDateTime;
import java.util.UUID;

import org.zahnleiter.casl4j.domain.exception.AbstractDomainException;

public final class DomainExceptionInWrongPackage extends AbstractDomainException {

    public DomainExceptionInWrongPackage(String message, UUID id, OffsetDateTime timeOfOccurrence) {
        super(message, id, timeOfOccurrence);
    }

}
