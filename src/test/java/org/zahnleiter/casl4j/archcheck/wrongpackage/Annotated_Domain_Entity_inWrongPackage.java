package org.zahnleiter.casl4j.archcheck.wrongpackage;

import org.zahnleiter.casl4j.domain.entity.DomainEntity;

/**
 * Clean Architecture Support Library for Java - Architecture Check
 * <br>
 * (c) 2021 Holger Zahnleiter, all rights reserved
 *
 * @author Holger Zahnleiter
 */
@DomainEntity
public class Annotated_Domain_Entity_inWrongPackage {

}
