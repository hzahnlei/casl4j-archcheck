package org.zahnleiter.casl4j.archcheck;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.File;

import org.gradle.api.Project;
import org.gradle.api.logging.Logger;
import org.gradle.testfixtures.ProjectBuilder;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

@Tag("IT")
@Tag("slow")
@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
final class DomainCheckGradlePluginIT {

    @Test
    void Plugin_can_be_accessed_by_name() {
        final var project = ProjectBuilder.builder().build();
        final var pluginUnderTest = new DomainCheckGradlePlugin();
        pluginUnderTest.apply(project);
        assertNotNull(project.getTasks().getByName("domainArchitectureCheck"));
    }

    @Test
    void Plugin_is_part_of_the_verification_group() {
        final var project = ProjectBuilder.builder().build();
        final var pluginUnderTest = new DomainCheckGradlePlugin();
        pluginUnderTest.apply(project);
        assertEquals("verification", project.getTasks().getByName("domainArchitectureCheck").getGroup());
    }

    /**
     * It is a little bit complex to test the plug-in. It needs access to some
     * classes. I did not find any other way but to mock Gradle Project's
     * {@link Project#getBuildDir()} so that it returns a path to this project's
     * classes. Otherwise the plug-in will be applied on an empty set of classes,
     * that is, the code will not be completely tested.
     * <p>
     * There is no good way to inspect the plug-in. However, the classes of this
     * project should not violate any architecture rules. We are checking for that.
     * Furthermore we are verifying the number of calls to log.debug(). This number
     * should be the number of classes in this projects Java main. Verifying the
     * count of method invocations is something I rarely do as this makes the test
     * very implementations specific. <b>This is nothing I would recommend.</b> In
     * this case I just had no better idea.
     * <p>
     * Also note that this test referrs to {@code build} as the class file folder.
     * Visual Studio Code creates a {@code bin} folder instead. However, if you run
     *
     * <pre>
     * ./gradlew assemble
     * </pre>
     *
     * then the {@code build} folder will be created and this test will not only
     * succed on command line or CI but also when run from within VS Code.
     */
    @Test
    void Plugin_can_be_applied() {
        final var project = ProjectBuilder.builder().build();
        final var mockProject = spy(project);
        when(mockProject.getBuildDir()).thenReturn(new File("build/classes/java/main"));
        final var mockLogger = mock(Logger.class);
        when(mockProject.getLogger()).thenReturn(mockLogger);
        final var pluginUnderTest = new DomainCheckGradlePlugin();
        pluginUnderTest.apply(mockProject);
        final var task = mockProject.getTasks().getByName("domainArchitectureCheck");
        assertNotNull(task);
        assertEquals(1, task.getActions().size());
        final var action = task.getActions().get(0);
        assertDoesNotThrow(() -> action.execute(task));
        verify(mockLogger, times(5)).debug(anyString(), anyString());
    }

    /**
     * This works on my command line and as a CI build. However, it does not work
     * when run with VS Code's Test Explorer. The problem seems to be there since
     * JDK 16: https://github.com/gradle/gradle/issues/18647. For now I stick with
     * Java 11.
     * <p>
     * However, there is another problem. This test does not work when I am
     * describing the plug-in within the {@code build.gradle} instead of the
     * traditional
     * {@code src/main/resources/META-INF/gradle-plugins/org.zahnleiter.casl4j.domain-architecture-check.properties}.
     * But I am preferring {@code build.gradle} over the additional properties files
     * as in this case everything is in one place rather than being scattered over
     * different places.
     * <p>
     * Hence, I have decided to disable this test for now.
     */
    @Test
    @Disabled("Does not work with JDK 16+, also requires properties file")
    void Domain_check_plugin_should_be_loadable() {
        final var project = ProjectBuilder.builder().build();
        project.getPluginManager() //
                .apply("org.zahnleiter.casl4j.domain-architecture-check");
        assertTrue(project.getPluginManager()
                .hasPlugin("org.zahnleiter.casl4j.domain-architecture-check"));
        assertNotNull(project.getTasks().getByName("domainArchitectureCheck"));
    }
}
