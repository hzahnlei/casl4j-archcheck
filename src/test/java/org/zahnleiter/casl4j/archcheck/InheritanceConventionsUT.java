package org.zahnleiter.casl4j.archcheck;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.zahnleiter.casl4j.domain.entity.Aggregate;
import org.zahnleiter.casl4j.domain.entity.DomainEntity;

import com.tngtech.archunit.core.importer.ClassFileImporter;

/**
 * Clean Architecture Support Library for Java - Architecture Check
 * <br>
 * (c) 2021 Holger Zahnleiter, all rights reserved
 *
 * @author Holger Zahnleiter
 */
@Tag("UT")
@Tag("fast")
@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
final class InheritanceConventionsUT {

    final ClassFileImporter classImporter = new ClassFileImporter();

    @DomainEntity
    @Aggregate
    final class SomeDomainEntityThatIsAnAggregate {
    }

    @Test
    void DomainEntity_can_be_Aggregate() {
        final var myClasses = this.classImporter.importClasses(SomeDomainEntityThatIsAnAggregate.class);
        assertDoesNotThrow(() -> InheritanceConventions.aggregatesAreDomainEntities.check(myClasses));
    }

    @Aggregate
    final class SomeAggregateThatIsNoDomainEntity {
    }

    @Test
    void Aggregates_have_to_be_DomainEntities() {
        final var myClasses = this.classImporter.importClasses(SomeAggregateThatIsNoDomainEntity.class);
        try {
            InheritanceConventions.aggregatesAreDomainEntities.check(myClasses);
            fail("Expected exception");
        } catch (final AssertionError cause) {
            assertEquals(
                    "Architecture Violation [Priority: MEDIUM] - Rule 'classes that are annotated with @Aggregate should be annotated with @DomainEntity' was violated (1 times):\n"
                            +
                            "Class <org.zahnleiter.casl4j.archcheck.InheritanceConventionsUT$SomeAggregateThatIsNoDomainEntity> is not annotated with @DomainEntity in (InheritanceConventionsUT.java:0)",
                    cause.getMessage());
        }
    }

}
