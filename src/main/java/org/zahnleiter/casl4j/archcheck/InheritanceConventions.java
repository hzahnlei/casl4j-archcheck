package org.zahnleiter.casl4j.archcheck;

import static com.tngtech.archunit.lang.syntax.ArchRuleDefinition.classes;

import org.zahnleiter.casl4j.domain.entity.Aggregate;
import org.zahnleiter.casl4j.domain.entity.DomainEntity;

import com.tngtech.archunit.lang.ArchRule;

/**
 * Checks to make sure the inheritance contraints are not violated.
 * <p>
 * Javadoc issues warnings as the rules are not documented. But I think the
 * ArchUnit DSL does a great job and is very readable indeed. Any comment would
 * only make it worth. Therefore I am ignoring these warnings.
 * <p>
 * Clean Architecture Support Library for Java - Architecture Check
 * <br>
 * (c) 2021 Holger Zahnleiter, all rights reserved
 *
 * @author Holger Zahnleiter
 */
public final class InheritanceConventions {

    private InheritanceConventions() {
    }

    public static final ArchRule aggregatesAreDomainEntities = classes() //
            .that().areAnnotatedWith(Aggregate.class) //
            .should().beAnnotatedWith(DomainEntity.class) //
            .allowEmptyShould(true); // Don't panic if no classes given.

}
