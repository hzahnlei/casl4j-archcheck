package org.zahnleiter.casl4j.archcheck;

import com.tngtech.archunit.core.domain.JavaClasses;
import com.tngtech.archunit.core.importer.ClassFileImporter;

import org.gradle.api.Plugin;
import org.gradle.api.Project;

/**
 * An abstract base class for Gradle architecture check plug-ins.
 * <p>
 * Clean Architecture Support Library for Java - Architecture Check
 * <br>
 * (c) 2021 Holger Zahnleiter, all rights reserved
 *
 * @author Holger Zahnleiter
 */
public abstract class AbstractArchCheckGradlePlugin implements Plugin<Project> {

    @Override
    public final void apply(final Project project) {
        final var checkTask = project.task(taskName())
                .doLast(task -> {
                    final var classImporter = new ClassFileImporter();
                    // Need Gradle's build directory so that we can access and check the project
                    // code.
                    final var buildDirPath = project.getBuildDir().getAbsolutePath();
                    final var classes = classImporter.importPath(buildDirPath);
                    project.getLogger().debug("{} will examine the following classes...", taskName());
                    for (final var clazz : classes) {
                        project.getLogger().debug("...arch check -> {}", clazz.getFullName());
                    }
                    architectureChecks(classes);
                });
        // Checks should be part of the verification group like "check" and "test".
        checkTask.setGroup("verification");
    }

    /**
     * The checks contained in a plug-in are executed by having Gradle run a task
     * with this name. Assume the task name would be "X", then
     *
     * <pre>
     * ./gradlew X
     * </pre>
     *
     * would execute the checks from {@link #architectureChecks(JavaClasses)}, if
     * applicable.
     *
     * @return Unique name of this Gradle task.
     */
    protected abstract String taskName();

    /**
     * Checks to make sure your implementation follows some architecture rules.
     *
     * @param classes to be checked.
     */
    protected abstract void architectureChecks(final JavaClasses classes);

}
