/**
 * <pre>
 *  #####    ###    #####  ##           ##      ##  A  C
 * ##   ##  ## ##  ##   ## ##          ##       ##  R  H
 * ##      ##   ## ##      ##          ##       ##  C  E
 * ##      #######  #####  ##         ##        ##  H  C
 * ##      ##   ##      ## ##        ##         ##     K
 * ##   ## ##   ## ##   ## ##        ##    ##   ##
 *  #####  ##   ##  #####  #######  ##      #####
 *
 * Clean Architecture Support Library for Java - Architecture Check
 * (c) 2021 Holger Zahnleiter, all rights reserved
 * </pre>
 *
 * Clean Architecture Support Library for Java - Architecture Check
 * <br>
 * (c) 2021 Holger Zahnleiter, all rights reserved
 *
 * @author Holger Zahnleiter
 */
package org.zahnleiter.casl4j.archcheck;
