package org.zahnleiter.casl4j.archcheck;

import com.tngtech.archunit.core.domain.JavaClasses;

/**
 * Concrete architecture check Gradle plug-in that does only the basic Clean
 * Asrchitecture checks. Other checks need to be implemented in framework
 * specific plug-ins. For example an extended plug-in for Spring applications
 * could check that a REST controller uses
 * {@link org.zahnleiter.casl4j.domain.usecase.UseCase}s but not
 * {@link org.zahnleiter.casl4j.domain.repository.DomainRepository}s directly.
 * But therfore we need knowledge of the DI framework, which we do not have in
 * this technology agnostic part of CASL4J.
 * <p>
 * Checks can be executed by running
 *
 * <pre>
 * ./gradlew domainArchitectureCheck
 * </pre>
 *
 * Clean Architecture Support Library for Java - Architecture Check
 * <br>
 * (c) 2021 Holger Zahnleiter, all rights reserved
 *
 * @author Holger Zahnleiter
 */
public final class DomainCheckGradlePlugin extends AbstractArchCheckGradlePlugin {

    @Override
    protected String taskName() {
        return "domainArchitectureCheck";
    }

    @Override
    protected void architectureChecks(final JavaClasses classes) {
        InheritanceConventions.aggregatesAreDomainEntities.check(classes);
        PackageConventions.aggregatesInDomainPackage.check(classes);
        PackageConventions.domainEntitiesInDomainPackage.check(classes);
        PackageConventions.domainExceptionInDomainExceptionPackage.check(classes);
        PackageConventions.domainTypesInDomainTypePackage.check(classes);
    }

}
