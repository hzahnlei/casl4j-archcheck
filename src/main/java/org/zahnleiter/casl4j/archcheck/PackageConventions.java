package org.zahnleiter.casl4j.archcheck;

import static com.tngtech.archunit.lang.syntax.ArchRuleDefinition.classes;

import org.zahnleiter.casl4j.domain.entity.Aggregate;
import org.zahnleiter.casl4j.domain.entity.DomainEntity;
import org.zahnleiter.casl4j.domain.entity.type.DomainType;
import org.zahnleiter.casl4j.domain.exception.AbstractDomainException;

import com.tngtech.archunit.lang.ArchRule;

/**
 * Checks to make sure the domain logic code does not violate the package
 * conventions.
 * <p>
 * Javadoc issues warnings as the rules are not documented. But I think the
 * ArchUnit DSL does a great job and is very readable indeed. Any comment would
 * only make it worth. Therefore I am ignoring these warnings.
 * <p>
 * Clean Architecture Support Library for Java - Architecture Check
 * <br>
 * (c) 2021 Holger Zahnleiter, all rights reserved
 *
 * @author Holger Zahnleiter
 */
public final class PackageConventions {

    private PackageConventions() {
    }

    static final String DOMAIN_PACKAGE_NAME = "..domain..";
    static final String DOMAIN_EXCEPTION_PACKAGE_NAME = "..domain.exception..";
    static final String DOMAIN_ENTITY_PACKAGE_NAME = "..domain.entity..";
    static final String DOMAIN_TYPE_PACKAGE_NAME = "..domain.type..";
    static final String DOMAIN_ENTITY_TYPE_PACKAGE_NAME = "..domain.type.entity..";

    public static final ArchRule domainExceptionInDomainExceptionPackage = classes() //
            .that().areAssignableTo(AbstractDomainException.class) //
            .should().resideInAnyPackage(DOMAIN_PACKAGE_NAME, DOMAIN_EXCEPTION_PACKAGE_NAME) //
            .allowEmptyShould(true); // Don't panic if no classes given.

    public static final ArchRule domainEntitiesInDomainPackage = classes() //
            .that().areAnnotatedWith(DomainEntity.class) //
            .should().resideInAnyPackage(DOMAIN_PACKAGE_NAME, DOMAIN_ENTITY_PACKAGE_NAME) //
            .allowEmptyShould(true); // Don't panic if no classes given.

    public static final ArchRule aggregatesInDomainPackage = classes() //
            .that().areAnnotatedWith(Aggregate.class) //
            .should().resideInAnyPackage(DOMAIN_PACKAGE_NAME, DOMAIN_ENTITY_PACKAGE_NAME) //
            .allowEmptyShould(true); // Don't panic if no classes given.

    public static final ArchRule domainTypesInDomainTypePackage = classes() //
            .that().areAnnotatedWith(DomainType.class) //
            .should().resideInAnyPackage(DOMAIN_PACKAGE_NAME, DOMAIN_TYPE_PACKAGE_NAME,
                    DOMAIN_ENTITY_PACKAGE_NAME, DOMAIN_ENTITY_TYPE_PACKAGE_NAME) //
            .allowEmptyShould(true); // Don't panic if no classes given.

}
