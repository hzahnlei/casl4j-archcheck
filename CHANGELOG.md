# Change Log

## 1.0.0 - Initial Version (2022-05-28)

### Functional

- Checks for basic conventions (definitely room to grow)
    - Inheritance
    - Package membership

### Non-Functional

- Gradle build script
- Publish module to GitLab Maven repository
- Produce coverage report and push to GitLab for display and merge requests
- Push test report to GitLab for display
